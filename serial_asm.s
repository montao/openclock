.equ uart_0, (0x860)

        .text
        .align 2
         .global out_char # visible for other files
         .global in_charx # visible for other files
out_char: movia r8, uart_0 # r8 pekar till basadress
outloop:  ldwio r9, 8(r8) # read status
		  andi r9, r9, 0x40 # mask  TxRdy
		  beq r9, r0, outloop # wait for value 1
		  andi r4, r4, 0x7f # kmaybe not necessary
		  stwio r4, 4(r8) # write a word / a byte
		  ret # and return

in_charx: movia r8, uart_0
inloop:	ldwio r9, 8(r8) #load status row
		andi r9, r9, 0x80 # mask bit 7
		beq r9, r0, noinput # iwait for value 1
		ldwio r2, 0(r8) # read a word
		andi r2, r2, 0x7f # maska 7 bitar
   		ret
noinput:movi r2, -1
		ret
