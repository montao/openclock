.equ timer, 0x920
.equ    timer1_status,        (0x920)         # status
.equ    timer1_control,       (0x924)         # control
.equ    timer1_period_low,    (0x928)         # period low
.equ    timer1_period_high,   (0x92C)         # period high
.equ    timer1_snap_low,      (0x930)         # snapshot low
.equ    timer1_snap_high,     (0x934)         # snapshot high
        .text
        .align 2
        .global checktimer # synlig i andra filer
checktimer:
         movi r2, -1     # defaultv�rde till R2
         movia r8, timer # R8 pekar p� timerkretsen
         ldwio r9,0(r8)  # status till R9
         andi r9, r9, 1  # Bara TimeOut-biten i R9
         beq r9,r0,NoTimeOut # hoppa om ingen timeout
         stwio r0,0(r8)  # nollst�ll TimeOut-biten
         mov r2,r0
NoTimeOut:
    ret
