#define SERIEPORT1 ( (volatile unsigned int *) 0x880 )
#define RXRDYBIT (1 <<7)
#define TXRDYBIT (1 << 6)
int rec_charx( void )
{
	volatile unsigned int * tmp = SERIEPORT1;
	if( (tmp[2] & RXRDYBIT) ) return( tmp[0] & 0xff);
	else return( -1 );
}

void send_char( int c )
{
	volatile unsigned int * tmp = SERIEPORT1;
	while( (tmp[2] & TXRDYBIT) == 0); /* v�nta tills TxRdy==1 */
	tmp[1] = c & 0xff;

}
