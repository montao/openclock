void puttime_hex( int * mytimep )
{
	//get each digit and call hex7seg with each digit at the lower end of the int thru bitshifting
int d0 = * mytimep;
int s0 = hex7seg(d0 & 0xf) & 0x7f;
int d1 = * mytimep >> 4;
int s1 = hex7seg(d1 & 0xf) & 0x7f;
int d2 = * mytimep >> 8;
int s2 = hex7seg(d2 & 0xf) & 0x7f;
int d3 = * mytimep >> 12;
int s3 = hex7seg(d3 & 0xf) & 0x7f;
int temp = 0;
temp = temp | s0;
temp = temp | (s1 << 7);
temp = temp | (s2 << 14);
temp = temp | (s3 << 21);
put_hexlow(temp);
}
