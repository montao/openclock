.equ timer, 0x920
.equ    timer1_status,        (0x920)         # status
.equ    timer1_control,       (0x924)         # control
.equ    timer1_period_low,    (0x928)         # period low
.equ    timer1_period_high,   (0x92C)         # period high
.equ    timer1_snap_low,      (0x930)         # snapshot low
.equ    timer1_snap_high,     (0x934)         # snapshot high
        .text
        .align 2
        .global init_timer # synlig i andra filer
init_timer:
          movia r8,timer  # basadressen till timern
          movia r9, 49999999 # R9 = 49999999 blir 1 sekund
          srli r10, r9, 16 # flytta h�ga bitar till h�gerkant
          stwio r10,12(r8)   # periodh = 0x0000
          stwio r9,8(r8)    # periodl = R9 (=0xC34F)
          movi r9,0b0110  # R9 = 0b0110 (continuous=1 start=1)
          stwio r9,4(r8)    # skriv till control
          ret




