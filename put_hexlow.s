.equ display, 0x9f0
.global put_hexlow

	.text
	.align 2

put_hexlow:
	movia r6, display
	stwio r4, 0(r6)
	ret
